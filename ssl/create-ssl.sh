#!/bin/sh

#Remove Any Existing Certs from Area
rm ca*
rm server*

# Generate Certificate Authority key

echo Generate Certificate Authority key
openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:2048 -out ca.key

# Generate the Certificate Authority certificate

echo Generate the Certificate Authority certificate
openssl req -new -x509 -days 360 -key ca.key -subj "/CN=StripeSample/O=000 riverBI Sandbox" -out ca.pem
openssl req -new -x509 -days 360 -key ca.key -subj "/CN=StripeSample/O=000 riverBI Sandbox" -out ca.crt

# Generate Server Key

echo Generate Server Key
openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:2048 -out server.key

# Generate the Server CSR

echo Generate the Server CSR
openssl req -new  -key server.key -subj "/CN=localhost/O=000 riverBI Sandbox" -out server.csr

# Generate the Server certificate

echo Generate the Server certificate
openssl x509 -days 360 -req -in server.csr -CAcreateserial -CA ca.crt -CAkey ca.key -out server.crt

echo 
echo --Server Generation Complete
echo 
