'use strict';
// Libraries and Modules used
const mongo = require('mongodb');
const exp   = require('express');
//const react = require('react-native');
const https = require('https');
const path  = require('path');
const fs    = require('fs');

//Server Settings
const app   = exp();
const dbport= 27107;
const srport= 2000;
const clientDir='pub';

//express server settings

var routes = {};
routes.productSel   = './pub/views/ShoppingPage.html'; 		// Products/Services to be selected
routes.shoppingCart = './pub/views/Cart.html';         		// Preparation of Purchase 
routes.checkout     = './pub/views/Checkout.html';    		// Card Details Gathering
routes.payConfirm   = './pub/views/paymentConfirmation.html'; 	// Payment Confirmation 

//routing server fc
var startServer = function(){
	//Express Settings for Files being Served
	app.use('/',exp.static(path.join(__dirname,'pub')));
	//views settings`	
	app.set('views',path.join(__dirname,'views'));
	//basic routing;
	app.get('/',function(req,res){
		res.sendFile(path.join(__dirname,'pub','views','Checkout.html'))
		console.log(fs.readFileSync(path.join(__dirname,'pub','views','Checkout.html')));
	});
	app.post('/charge',function(req,res){
		console.log(req);
		res.send('We Have Received Confirmation that you would like to make a payment');
	});
	const httpsOptions = {
		cert: fs.readFileSync(path.join(__dirname, 'ssl', 'server.crt')),	
		key: fs.readFileSync(path.join(__dirname, 'ssl', 'server.key')),
	}	
	https.createServer(httpsOptions,app).listen(srport,function(){
		console.log('Server '+clientDir+'/ on at https://localhost:'+srport)
	})
}



startServer();

